/*
Diese Funktion soll das Zeichen bei jedem klick neu positionieren. Die Bewgungsfreiheit wurde durch die Größe
des Divs festgelegt.
 */
var startTime = new Date().getTime();
document.getElementById("bewegendeBox").onclick = function (){
    document.getElementById("bewegendeBox").style.backgroundColor = "blue";
    var min_height = 100;
    var max_height = 400;
    var x_height = Math.round(Math.random() * (max_height - min_height)) + min_height;
    var min_width = 100;
    var max_width = 1000;
    var x_width = Math.round(Math.random() * (max_width - min_width)) + min_width;
    var min_size_width = 20;
    var max_size_width = 100;
    var x_size_width = Math.round(Math.random() * (max_size_width - min_size_width)) + min_size_width;
    var min_size_height = 20;
    var max_size_height = 100;
    var x_size_height = Math.round(Math.random() * (max_size_width - min_size_width)) + min_size_width;
    document.getElementById("bewegendeBox").style.marginTop = x_height+"px";
    document.getElementById("bewegendeBox").style.marginLeft = x_width+"px";
    document.getElementById("bewegendeBox").style.width = x_size_width+"px";
    document.getElementById("bewegendeBox").style.height = x_size_height+"px";
    var endTime = new Date().getTime()
    var dauer = endTime - startTime;
    document.getElementById("time").innerHTML = dauer +" millisec";
    startTime = endTime;
}

